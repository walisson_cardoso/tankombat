extends "Tank.gd"

signal player_died
var Mine = preload("res://Scenes/Mine.tscn")
var mine_of_mine = null # Variable to hold mine
var mine_rf = null      # Weak reference to mine
var isVisible = false   # Player is invisible (title and game over screens)

func _ready():
	hide() # Hides player on title screen
	._ready()
	SPEED += 40 # Increase players speed

func _process(delta):
	# Only action to be executed even if player is moving
	if Input.is_action_just_pressed("Mine"):
		place_mine()

func start():
	# To be called on main
	show()
	isVisible = true
	mine_of_mine = null
	mine_rf = null
	direction = 0
	set("global_rotation",0) # Player may be rotate due the last game. Puts it back on its original orientation

# Overrides read_input from basic Tank class
func read_input():
	# Not ready yet
	if !isVisible:
		return
	
	var action = "none"
	
	if Input.is_action_just_pressed("ui_select"):
		shoot()
	# What should the tank do?
	if Input.is_action_pressed("ui_up"):
		action = "walk_foward"
	elif Input.is_action_pressed("ui_down"):
		action = "walk_backward"
	elif Input.is_action_pressed("ui_right"):
		action = "turn_right"
	elif Input.is_action_pressed("ui_left"):
		action = "turn_left"
	else:
		action = "none"
	# Do the action and also locks input reading
	if action != "none":
		perform_action(action)

func place_mine():
	# If mine is freed or not instantiated yet, place another mine
	if (mine_rf == null or !mine_rf.get_ref()) and isVisible:
		mine_of_mine = Mine.instance()
		mine_rf = weakref(mine_of_mine)
		get_parent().add_child(mine_of_mine)
		mine_of_mine.position = position

#Bullet entered
func _on_Player_body_entered(body):
	if body.has_method("hit"):
		body.hit()
	hit()

#Enemy entered
func _on_Player_area_entered(area):
	if area.is_in_group("ENEMY"):
		if area.has_method("hit"):
			area.hit()
		hit()

func hit():
	.hit() # Explosion
	hide()
	isVisible = false
	if mine_rf != null and mine_rf.get_ref(): # If any mine is on field
		mine_of_mine.queue_free()
	emit_signal("player_died") # Player didn't actually die, just became invisble