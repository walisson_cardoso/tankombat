extends Area2D

var SPEED = 40
var BULLETSPEED = 100
var GRIDSIZE = 100
var SIDE = 40 # Space left on the side
var TOP = 60  # Space left on the top

var Bullet = preload("res://Scenes/Bullet.tscn")
var Explosion = preload("res://Scenes/Explosion.tscn")

signal action_complete
var screenSize

enum COORD {NORTH, EAST, SOUTH, WEST}

var moving = false
var angle = 0
var target_angle = 0 # How much it still need to rotate
var direction = COORD.NORTH # Direction the tank is facing
var next = direction # Next direction to face on a rotation process
var shootCounter = 0 # Similar to a Timer, but made on process() function

var target_x # Next x position after walking foward or backward
var target_y # Next y position after walking

var gridCoord_x = 0 # x coordinate translated to grid position
var gridCoord_y = 0 # y coordinate trnaslated to grid position

func _ready():
	screenSize = get_viewport_rect().size
	
	position.x = SIDE # Dummy position for all tanks
	position.y = TOP

func _process(delta):
	# What I said about timer...
	if shootCounter > 0:
		shootCounter -= delta
	
	if not moving:
		# If the tank is not moving, read a new instruction
		update_coordinates()
		read_input()
	else:
		# It is actually moving...
		move(delta)
	
func read_input():
	# To be overloaded
	var action = "none"
	#Actions: "walk_foward", "walk_backward", "turn_right", "turn_left"
	
	if action != "none":
		perform_action(action)

func perform_action(action):
	
	target_x = position.x
	target_y = position.y
	# Depending on its orientation, the tank will increment a specific coordinate position
	var x_target_inc = [0, GRIDSIZE, 0, -GRIDSIZE]
	var y_target_inc = [-GRIDSIZE, 0, GRIDSIZE, 0]
	# Wheel move doesn't look good was I wanted, but that's ok
	# Each wheel shoud move to a direction depending on the movement, but
	# enemies have a problem on its turning movement that I'm not really
	# interested on investigating
	$Wheel_l.flip_v = false
	$Wheel_r.flip_v = false
		
	if   action == "walk_foward":
		target_x = position.x + x_target_inc[direction]
		target_y = position.y + y_target_inc[direction]
	elif action == "walk_backward":
		target_x = position.x - x_target_inc[direction]
		target_y = position.y - y_target_inc[direction]
		$Wheel_l.flip_v = true
		$Wheel_r.flip_v = true
	elif action == "turn_right":
		target_angle = PI/2
		direction += 1
		if direction >= COORD.size():
			direction = 0
		$Wheel_l.flip_v = true
	elif action == "turn_left":
		target_angle = -PI/2
		direction -= 1
		if direction < 0:
			direction = COORD.size()-1
		$Wheel_r.flip_v = true
	
	angle = 0
	# Verifications to find out if there's a stone or an enemy in front of this tank
	var next_coord = pos_to_coord(target_x, target_y)
	var coord_ok = check_coordinate_enemies(next_coord[0], next_coord[1])
	var pos_ok = pos_is_free(target_x, target_y)
	
	# Verifies if movement would leed the tank out of bounds
	if target_x >= SIDE and target_x <= screenSize.x - SIDE and target_y >= TOP and target_y <= screenSize.y - TOP and pos_ok and coord_ok:
		$Wheel_l.play()
		$Wheel_r.play()
		moving = true
		# Actually only player will have "AudioMove". This shit is too loud
		if has_node("AudioMove") and (action == "walk_foward" or action == "walk_backward"):
			$AudioMove.play()
		elif has_node("AudioTurn") and (action == "turn_right" or action == "turn_left"):
			$AudioTurn.play()
	else:
		moving = true
		action = "none"
		target_x = position.x
		target_y = position.y

func move(delta):
	# This function is alittle messy
	# Verifies the difference between target position and actual position
	var posDiff = position - Vector2(target_x,target_y)
	position -= posDiff.normalized()*SPEED*delta
	
	# Target angle is how much the tank still needs to turn
	var dec = 0.02*SPEED*delta
	if target_angle < 0:
		dec = -dec
	rotate(dec)
	target_angle -= dec
	
	# If target moving is close enough, round it
	if posDiff.length() < 5 and abs(target_angle) < 0.05:
		update_coordinates()
		#Fix small differences made by the aproximated moviment
		position.x = gridCoord_x*GRIDSIZE+SIDE
		position.y = gridCoord_y*GRIDSIZE+TOP
		
		var angle = get("global_rotation")
		angle = round(angle/(PI/4))*(PI/4)
		set("global_rotation", angle)
		
		$Wheel_l.stop()
		$Wheel_r.stop()
		moving = false

func update_coordinates():
	# Updates grid coordinates
	var coord = pos_to_coord(position.x, position.y)
	gridCoord_x = coord[0]
	gridCoord_y = coord[1]

func pos_to_coord(x, y):
	# Coordinates to grid coordinates
	var grid_x = round((x-SIDE)/GRIDSIZE)
	var grid_y = round((y-TOP)/GRIDSIZE)
	return [grid_x, grid_y]

func shoot():
	# Shoots to a direction depending on the tank heading
	if shootCounter <= 0:
		var orient = [-PI/2, 0, PI/2, PI]
		var x_inc = [0, 55, 0, -55] # We want to shoot in front of the cannon exit
		var y_inc = [-55, 0, 55, 0]
		
		var bullet = Bullet.instance()
		bullet.position = Vector2(position.x+x_inc[direction], position.y+y_inc[direction])
		bullet.rotation = orient[direction]
		bullet.set_linear_velocity(Vector2(BULLETSPEED,0).rotated(orient[direction]))
		get_parent().add_child(bullet)
		
		# Two seconds until next bullet
		shootCounter = 2
		if has_node("AudioStreamPlayer"):
			$AudioStreamPlayer.play()

# Asks parent node if a stone is in the position
func pos_is_free(target_x, target_y):
	if get_parent().has_method("check_position"):
		return get_parent().check_position(target_x, target_y)
	else:
		return true # No parent, no problem

func check_coordinate_enemies(x,y):
	if get_parent().has_method("check_coordinate_enemies"):
		return get_parent().check_coordinate_enemies(x, y)
	else:
		return true # No parent, no problem

func hit():
	var explosion = Explosion.instance()
	explosion.position = position
	get_tree().get_root().add_child(explosion)