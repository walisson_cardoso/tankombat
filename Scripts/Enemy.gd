extends "Tank.gd"

var player = null   # No player found (Scene loaded alone)
var difficultyPerc = 0.0 # How smart the tank is
var lastAction = "" # Remembers last action
var enemyType = 1   # Enemy of type 1 will approach player first by y coordinate, enemy of type 2, by x.

func _ready():
	._ready()
	enemyType = (randi() % 2)+1
	add_to_group("ENEMY")
	if get_parent().has_node("Player"):
		player = get_parent().get_node("Player")

# Overrides read_input from basic Tank class
func read_input():
	var action = "none"
	var actions = ["walk_foward", "walk_backward", "turn_right", "turn_left"]
	
	var actionProb = rand_range(0,1)
	var shootProb = 0.7
	# Technically player will never be equals null in actual game, but better be prevented for future necessities
	if player == null or actionProb < 1-difficultyPerc: #Decreases when the game gets harder
		# Moves randomly
		action = actions[randi() % actions.size()]
		if (action == "turn_left" and lastAction == "turn_right") or (action == "turn_right" and lastAction == "turn_left") or (action == "walk_foward" and lastAction == "walk_backward") or (action == "walk_backward" and lastAction == "walk_foward"):
			action = actions[randi() % actions.size()] #Tries one time to do not repeat action. The algorithm is silly, but we don't need to take to this level
	else:
		# Approaches player
		# I found easier to work with coordinates and orientation, but certanly could use smarter approaches
		if player.gridCoord_y == gridCoord_y and player.gridCoord_x > gridCoord_x:
			# Player is in the same colum, but below
			if direction == COORD.NORTH or direction == COORD.WEST:
				action = "turn_right"
			elif direction == COORD.SOUTH:
				action = "turn_left"
			else:
				action = "walk_foward"
				if rand_range(0,1) < shootProb:
					shoot()
		elif player.gridCoord_y == gridCoord_y and player.gridCoord_x < gridCoord_x:
			# Player is in the same column, but above
			if direction == COORD.NORTH or direction == COORD.EAST:
				action = "turn_left"
			elif direction == COORD.SOUTH:
				action = "turn_right"
			else:
				action = "walk_foward"
				if rand_range(0,1) < shootProb:
					shoot()
		elif player.gridCoord_x == gridCoord_x and player.gridCoord_y > gridCoord_y:
			# Player is in the same row but at right
			if direction == COORD.NORTH or direction == COORD.WEST:
				action = "turn_left"
			elif direction == COORD.EAST:
				action = "turn_right"
			else:
				action = "walk_foward"
				if rand_range(0,1) < shootProb:
					shoot()
		elif player.gridCoord_x == gridCoord_x and player.gridCoord_y < gridCoord_y:
			# Player is in the same row, but left
			if direction == COORD.SOUTH or direction == COORD.EAST:
				action = "turn_left"
			elif direction == COORD.WEST:
				action = "turn_right"
			else:
				action = "walk_foward"
				if rand_range(0,1) < shootProb:
					shoot()
		else:
			# Player is not in the same column, neighter same row, so it reduces the distance walking
			# on the grid
			if enemyType == 1:
				action = approx_by_column()
			else:
				action = approx_by_row()
			
	if action != "none":
		lastAction = action
		perform_action(action)

func approx_by_column():
	var action = "none"
	if player.gridCoord_y > gridCoord_y:
		if direction == COORD.NORTH or direction == COORD.EAST:
			action = "turn_right"
		elif direction == COORD.WEST:
			action = "turn_left"
		else:
			action = "walk_foward"
	else:
		if direction == COORD.SOUTH or direction == COORD.EAST:
			action = "turn_left"
		elif direction == COORD.WEST:
			action = "turn_right"
		else:
			action = "walk_foward"
	return action

func approx_by_row():
	var action = "none"
	if player.gridCoord_x > gridCoord_x:
		if direction == COORD.NORTH or direction == COORD.WEST:
			action = "turn_right"
		elif direction == COORD.SOUTH:
			action = "turn_left"
		else:
			action = "walk_foward"
	else:
		if direction == COORD.NORTH or direction == COORD.EAST:
			action = "turn_left"
		elif direction == COORD.SOUTH:
			action = "turn_right"
		else:
			action = "walk_foward"
	return action

#Bullet entered
func _on_Enemy_body_entered(body):
	if body.has_method("hit"):
		body.hit()
	hit()

func _on_Enemy_area_entered(area):
	if area.has_method("hit") and not area.is_in_group("STONE"):
		area.hit()
	hit()

func hit():
	.hit()
	queue_free()