extends RigidBody2D

var Explosion = preload("res://Scenes/Explosion.tscn")

func _ready():
	pass

# Function to be called for other objects
func hit():
	_on_Explosion_timeout()

# If the bullet did hit nothing, auto explodes
func _on_Explosion_timeout():
	var explosion = Explosion.instance()
	explosion.position = position
	get_tree().get_root().add_child(explosion)
	queue_free()

# Bullet hitting a second bullet
func _on_Bullet_body_entered(body):
	if body.has_method("hit"):
		body.hit()
		_on_Explosion_timeout()
