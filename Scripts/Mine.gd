extends Area2D

func _ready():
	$AnimatedSprite.play()
	position = Vector2(100,100)

func _on_Mine_area_entered(area):
	if area.is_in_group("ENEMY"):
		# Destroy enemies. Heck yeah!
		area.hit()
		queue_free()

func _on_AutoDestroy_timeout():
	$AnimationPlayer.play("fadeout")

func _on_AnimationPlayer_animation_finished(anim_name):
	# Disapears after fading out
	queue_free()
