extends Node

export (PackedScene) var Stone
export (PackedScene) var Enemy

var score = 0
var bestScore = 0
var stones = []
var stone_pos = [Vector2(340,360), Vector2(140,160), Vector2(140, 460)]
var enemies = []
var enemies_wr = []
var spawnFlag = []
var difficultyPerc = 0.1
var choice = 0

func _ready():
	randomize()
	$AudioSnare.play()
	$Player.position = $PlayerPosition.position
	#Wait for start button to be pressed (HUD node)
	

func new_game():
	$AudioSnare.stop()
	$Player.position = $PlayerPosition.position
	$Player.start() #Some few operations on player node
	
	# Prepares HUD
	$HUD.new_game()
	# Restarts difficulty and scores
	difficultyPerc = 0.1
	choice = 0 # Auxiliar to difficulty
	score = 0
	$HUD/Score.text = str(score)
	
	# Reinstantite stone on game
	stones = []
	for pos in stone_pos:
		var stone = Stone.instance()
		stone.position = pos
		stones.append(stone)
		add_child(stone)
	
	enemies = []    # At most four enemies
	enemies_wr = [] # References to enemies, so we avoid messing with freed instances
	spawnFlag = []  # If spawn() was called, do not call again, it will be spawnmed in 5 seconds
	enemies.append(null)
	spawn(0)        # Spawn the first enemy created
	enemies_wr.append(weakref(enemies[0])) # Its reference
	spawnFlag.append(false) # Not schreduled to spawn, but the enemy is present
	$VerifyEnemies.start()  # Continuously verify if enemies were destroyed
	$IncreaseDifficulty.start()

func game_over():
	#Connected to player_died signal
	for i in range(enemies_wr.size()):
		if enemies_wr[i].get_ref():
			get_node("Spawn" + str(i+1) + "/SpawnTimer").stop()
			$VerifyEnemies.stop()
			$IncreaseDifficulty.stop()
			enemies[i].queue_free()
	
	if score > bestScore:
		bestScore = score
	$HUD/Best.text = "Best: " + str(bestScore)
	$HUD.show_game_over()

func check_position(x, y):
	# Function to help children to know if a stone is on position (x,y)
	# Could work with grid coordinates, but I was to lazy to implement this on stones
	var canGo = true
	for stone in stones:
		if stone.is_inside(x,y):
			canGo = false
	return canGo
	
func check_coordinate_enemies(x,y):
	# Check if enemies are on grid coordinates (x,y)
	# Note, though, enemies walking to (x,y) did not reach this position yet
	# The player can use this in its favor to destroy enemies
	var canGo = true
	for i in range(enemies_wr.size()):
		if enemies_wr[i].get_ref():
			if enemies[i].gridCoord_x == x and y == enemies[i].gridCoord_y:
				canGo = false
	return canGo

func spawn(index = -1):
	# Left upper corner, right upper corner, left bottom corner, right bottom corner
	var pos = [$Spawn1.position, $Spawn2.position, $Spawn3.position, $Spawn4.position]
	var rot = [PI, PI, 0, 0]
	var dir = [2, 2, 0, 0]
	
	if index == -1:
		index = randi() % pos.size()
	
	enemies[index] = Enemy.instance()
	add_child(enemies[index])
	
	enemies[index].position = pos[index]
	enemies[index].rotate(rot[index])
	enemies[index].direction = dir[index]

func _on_VerifyEnemies_timeout():
	# Periodically verifies if enemies were destroyed
	for i in range(enemies_wr.size()):
		if !enemies_wr[i].get_ref() and not spawnFlag[i]:
			# If yes, player earns one point and enemy is schreduled to spawn
			score += 1
			$HUD/Score.text = str(score)
			spawnFlag[i] = true
			get_node("Spawn" + str(i+1) + "/SpawnTimer").start()

func _on_SpawnTimer_timeout(timerNumber):
	# TimerNumber parameter is set on the Timers from the different spawn locations
	if timerNumber > enemies.size():
		return
	spawn(timerNumber)
	enemies[timerNumber].difficultyPerc = difficultyPerc
	enemies_wr[timerNumber] = weakref(enemies[timerNumber])
	spawnFlag[timerNumber] = false
			

func _on_IncreaseDifficulty_timeout():
	# Alternates between increasing difficulty and number of enemies
	choice = (choice + 1) % 2
	if choice == 0 and enemies.size() < 4:
		var index = enemies.size()
		enemies.append(null)
		spawn(index)
		enemies_wr.append(weakref(enemies[index]))
		spawnFlag.append(false)
	elif difficultyPerc < 0.9:
		difficultyPerc += 0.1

