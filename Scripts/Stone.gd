extends Area2D

var durability = 10

func _ready():
	add_to_group("STONE")

func is_inside(x, y):
	# Is point (x,y) inside this stone?
	if durability <= 0:
		# If stone was destroyed
		return false
	
	if x > position.x-50 and x < position.x+50 and y > position.y-50 and y < position.y+50:
		return true
	else:
		return false

func hit():
	durability -= 1
	
	if durability <= 0:
		# Stone was destroyed
		set("modulate", Color(0,0,0,0.3)) # Smooth shadow on its location
	else:
		set("modulate", Color(1,durability/10.0,durability/10.0,1)) # Makes it more red every hit

# Hit by bullet
func _on_Stone_body_entered(body):
	if body.has_method("_on_Explosion_timeout"):
		body._on_Explosion_timeout()
		hit()
