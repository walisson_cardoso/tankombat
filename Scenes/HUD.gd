extends CanvasLayer

signal press_start
var isPlaying = false

func _ready():
	# Flags weren't being disabled on editor
	get_node("Title").get_texture().set_flags(2)
	$ColorRect.hide()
	$Score.hide()
	$GameOver.hide()
	$Best.hide()
	$Start.show()
	$FullScreen.show()
	
func _process(delta):
	#Pause Game
	if Input.is_action_just_pressed("ui_accept"):
		if isPlaying:
			if get_tree().paused:
				get_tree().paused = false
				$Title.hide()
				$ColorRect.hide()
				$GameOver.text = "Game Over"
				$GameOver.hide()
			else:
				get_tree().paused = true
				$Title.show()
				$ColorRect.show()
				$GameOver.text = "Paused"
				$GameOver.show()
		else:
			# Same as click start button
			_on_Start_pressed()

# To be called on main
func new_game():
	$Title.hide()
	$ColorRect.hide()
	$Score.show()
	$GameOver.hide()
	$Best.hide()
	$Start.hide()
	$FullScreen.hide()
	isPlaying = true
# To be called on main
func show_game_over():
	$ColorRect.show()
	$GameOver.show()
	$Best.show()
	$Start.show()
	isPlaying = false

func _on_Start_pressed():
	emit_signal("press_start")

func _on_FullScreen_pressed():
	OS.set_window_fullscreen($FullScreen.pressed)
